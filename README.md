# kata-devops-3tier

<img src="frontend/src/assets/images/beer.jpg" width="20%" alt="Project image">

## Prerequisites

- We configure 2 types of owned local runners : 
    * One for the building of images that we called : **runner-build-3tier**
    * One for hosting kubernetes cluster : **runner-deploy-3tier**
- We configure a Kubernetes cluster with Minikube , 3 namespaces to host dev,uat,prod , and an account service for each to clearly manage runners rights.
- You need to have at least one **tag** to begin to push in develop (the previous tag is used for naming when pushing in develop without tag).

## Lifecycle

### stages
- **app_build** (Compile the Angular and NodeJS and ensure the developpers didn't inserted deployment bugs) => These steps create Artifacts
- **image_build** (Jobs inside this stage are building images with the previous artifacts, making the apps portable)
- **generate_config** (Generate a value.yml artifact based on context and used for Helm global variables.)
- **deploy** (Get the images previously built and deploy them in Kubernetes with right permissions and tags)
- **destory_if_needed** (Destroy short lived environment)

### Reduce pipeline time
I used shared runners in the build parts because my own runners are not very powerfull (Low memory,Low CPU)
I passed the build part from ~15min of execution to ~2min. That's easier to test.

### Reduce pipeline triggering
In develop mode without tagging , I made a lifecycle on the frontend/backend directories to trigger jobs when it's usefull. Don't trigger when only README modified.

### When the pipelines are triggered ?

- **Develop no tag** : 
    * if: '$CI_COMMIT_REF_NAME == "develop" && ($CI_COMMIT_TAG == null || $CI_COMMIT_TAG == "")'
    * Let you deploy your application in shortlived mode and allow developpers to deploy testing environment. Ensure that the triggering is not from a tag action.
- **Develop with tag** : 
    * if: '$CI_COMMIT_REF_NAME == "develop" && $CI_COMMIT_TAG =~ /[d+].[d+].[d+]-rc*/'
    * Let you deploy your application when tagging branch and allow releasing the application
- **Main with tag** : 
    * if: '$CI_COMMIT_REF_NAME == "main" && $CI_COMMIT_TAG =~ /[d+].[d+].[d+]$/'
    *  Deploy your application in production mode when tagging branch in main.

### Why this lifecycle ? 

- /[d+].[d+].[d+]/' was previously /v[d+].[d+].[d+]/' but we use the tag of branch to pass it to helm. This allow integrity between CI/CD and Kubernetes versioning.
- I take the decision to parallelize the application compile jobs (No need to wait the backend build before the front build). Angular and NodeJS are builded at the same time.
- The images are builded in parallel too.
- I decided to add a step to generate a value.yml artifact based on context and used for Helm global variables. In the future , is there is a lot of global variables to pass through Helm, no need to polluate the gitlab-ci.yml file to pass them in each job. It also permits to quickly see mutualized variables / specific variables following pipeline context. The value.yml is a template and can be easily modified.
- The deploy mode is not the same when pushing commit to **develop**, when tagging in **develop** or tagging in **main**. Different namespaces are used. The value.yml file is used from artifacts.

## Where is the DB ?

- It's for the moment hosted on AWS POC account with low data inside. It's a managed Postgrl RDS.

## Install giltab runner for Docker

- [Install runner for Docker](https://docs.gitlab.com/runner/install/)

## Install giltab runner for Kubernetes

- [Install kubectl/adm](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
- [Install minikube](https://kubernetes.io/fr/docs/tasks/tools/install-minikube/)
- Minikube start
- Create namespaces for dev,uat,prod
- ( Create/set context for minikube ,for local interactivity )
- [Install runner for Kubernetes](https://docs.gitlab.com/runner/install/kubernetes.html)

## Note to create a Service dedicated to giltab runner in kubernetes :

- kubectl create sa sa-runner -n NAMESPACE
- kubectl create role sa-runner-role -n NAMESPACE ----verb=get,list,watch,create,delete,patch,update --resource=pods,secret,pods/exec,pods/attach
- kubectl create rolebinding sa-runner-rolebinding -n NAMESPACE --role=sa-runner-role --serviceaccount=NAMESPACE:sa-runner

**Use the name of the service** (ex:sa-runner) into the giltab-ci.yml jobs when running on Kubernetes

## Future 
- We later plan to pass to AWS ( EC2/ECS ) to have multiple runners mounted with Golden AMI.
- We assume that these repository is not sensible and we pass password without hiding it. We later plan to pass to Hashicorp Vault.
- Why only two branches ? Because this is a short-project and don't need a huge industrialization.
    * We assume that there is no features branches to merge in Develop. So everything is tested when commit inside the environement
    * In real life , features branches must be integrated as a merge request that trigger the build of a short-lived environement. Because each developper need to have access to a short-lived environement while ensuring this previous one is not being overrided by an other merge request.
    * We could then incorporate a dev-quali branch while merging in Develop. Meaning that there is : 1 PROD env , 1 UAT env , 1 DEV-Quali env , multiple short-lived environements per merge request.
- We could imagine to have different Clusters to separate environements.
- One Postrgl for prod , one for uat , one for dev-quali/short-lived.
- All the landing-zone is created by hand , I tried to industrialize with Terraform and OSS hypervizor providers but it's not really conclusive. It's better to create a landing zone one Cloud Providers (like AWS) and use managed services.